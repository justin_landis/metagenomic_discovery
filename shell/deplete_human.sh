#!/bin/bash -l

module load bbmap -q


BBM_PATH=`which bbmap.sh`
# EDIT ME!! ---- This pathway does not exist in Penguin. 
REF_GENOME="hg38_UCSC"
BBM_INDEXES="/home/public/reference_genomes/${REF_GENOME}/Sequence/BBMapIndex"
NUM_PROCS=48
BBM_ARGS="unpigz=t pigz=t t=${NUM_PROCS}"

# check if BBMap is working
if [ ! -x $BBM_PATH ]; then
echo "BBMap not loaded correctly. Please check that $BBM_PATH exists"
	exit 1
fi

# check if there are even any files given
if [ $# -eq 0 ]; then
echo "Please provide at least one fastq.gz!"
	exit 1
fi

#perhaps- while [[ $# -gt 1 ]]
for filename in ${@}; do
key="$1"
case $key in
        # parse -n into the number of processors to use
        -f|--force)     #Overwrite - ask aubrey to explain
		delete_existing=1        #Would this cause a problem when running script multiple times?
					 # If delete_existing lives in a local environment, will it always be 1?
	;;
	*)  # treat everything else as a filename
		#Currently, the last file of the fastq is not read as a file.
		#make sure we have real files
		if [[ ! -f $1 ]];
		then
			echo "ERROR: $1 is not a file"
			exit
		else	 
			dname=$(dirname ${filename})
			#if ends in .gz ...
			if [ ${1: -3} == ".gz" ]; 
			then
				fname=$(basename ${filename} .fastq.gz)
				in=${dname}/${fname}.fastq.gz
				out=${dname}/${fname}_${REF_GENOME}_depleted.fastq.gz
			else
                		fname=$(basename ${1} .fastq)
				in=${dname}/${fname}.fastq
				out=${dname}/${fname}_${REF_GENOME}_depleted.fastq
			fi
		fi
		

#                      
#               

                # if output file already exists, just exit
                if [[ -f $out ]]; then
			if [[ $delete_existing == 1 ]]; then
				BBM_ARGS="$BBM_ARGS overwrite=t"
			else
        	                echo "ERROR: $out already exists! Use -f to force overwrite."
	                       	exit
			fi
                fi

                #make sure we have real files
                if [[ ! -f ${in} ]]; then
#                       	echo "file ${in} does not exist"
                        exit
		else

			submission="${BBM_PATH} ${BBM_ARGS} \
			path=${BBM_INDEXES} \
			in=${in} \
			outu=${out} &> ${fname}_${REF_GENOME}_depleted.log"
			echo $submission
		fi
            ;;
esac
shift
done
