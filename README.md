# README #



### What is this? ###

This repository is the Dittmer lab's virus discovery pipeline for our CentOS system. Three of the quality control shell scripts are taken from the private [vironomics](https://bitbucket.org/dittmerlab/vironomics/src/master/) repository on the Dittmer lab's bitbucket.
These scripts are highly specialized to our own computing system and may need to be modified when run on a different system.

The [source code](https://bitbucket.org/justin_landis/metagenomic_discovery/src/master/) should be public for now.

### How to Run ###

A script has not yet been finalized. ```path``` refers to the full system path to this git repository.  Assuming the working directory contains multiple .fastq.gz files, the general pipeline is as follows:

```{bash}
path/trim.sh *.fastq.gz | bash
path/deplete_human.sh *_deadapt.fastq.gz | bash
path/deplete_ribo.sh *hg38_UCSC_depleted.fastq.gz | bash

module load fastx_toolkit

ls *ribo_depleted.fastq.gz | xargs -I{} basename {} .fastq.gz | xargs -I{} echo "gunzip -c {}.fastq.gz | fastq_to_fasta -Q33 -o {}.fasta" | bash

module load megahit
#first pass
ls *ribo_depleted.fasta | xargs -I{} basename {} .fasta | xargs -I{} echo "megahit --num-cpu-threads 12 --memory 400000000000 -r {}.fasta -o {}_megahit_assembly &> {}_megahit_assembly.out" | bash

#second pass with MaSurCA
# that goes here

module load blast
#blastn for results

ls -d *_megahit_assembly | xargs -I{} basename {} | xargs -I{} echo "blastn -db /home/public/blastdbs/nt -num_threads 12 -max_target_seqs 1 -outfmt '6 qseqid sseqid evalue bitscore pident length sgi sacc staxids sscinames scomnames stitle' -query {}/final.contigs.fa -out {}_vs_nt.blastout &> ./{}_megahit_final_contings_blastNT.out" | bash

#view blast results
grep -viE "homo|Human|Gorilla|troglodytes|pongo|Macaca|leucogenys|18S" *.blastout

```

### Requirements ###

* [This git repository](https://bitbucket.org/justin_landis/metagenomic_discovery/src/master/)
* bbmap module
* fastx_toolkit module
* megahit module
* [MaSuRCA](https://github.com/alekseyzimin/masurca/releases)
* blast module

### Explaination of scripts ###

#### trim.sh ####

This script utilizes bbmap module by executing the [bbduk.sh](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbduk-guide/) script.

* ADAPTER_PATH - path to adapters.fa file located in this repository. By default adapters.fa is assumed to be located in the user's home directory. They should either move it there or change the script to point to this file.
* BBDUK_ARGS
    * k - kmer length. Default set to 23
    * mink - minimum kmer length to match. Default set to 11
    * ktrim - which side to trim for kmer (r or l). When a reference kmer matches a read kmer, that kmer is trimmed and all bases to the direction specified by ktrim.
    * hdist - number of mismatches to allow
    * minlength - minimum read length after trimming
    * trimq - Quality Phred score to keep
    * qtrim - which side to trim for quality (r or l)
    * ftl - force trim the first x number of bases
    * ftr - force trim everything after base x
    * maq - average Phred score to keep (on the trimmed read)

#### deplete_human.sh ####

This script also utilizes bbmap module by executing bbmap.sh. This script is fairly straight forward. The only thing that may need to be changed is the path saved to BBM_INDEXES.
This is set to a directory on our local machine that points to the *hg38_UCSC* BBmap indexes. This directory is approximately 12Gb in size. Reads that map to hg38_UCSC are discarded.

#### deplete_ribo.sh #### 

In functionality, this script is almost identical to deplete_human.sh. This script only differs in the fact that the reference is a directory dubbed as *all_silva_ribo*. This directory is approximately 7.1Gb in size. Reads that map to these references are also discarded.

#### fastq_to_fasta ####

Another filtering step on quality Phred score of at least 33. This also converts files into fasta files.

#### megahit ####

Generates contigs from kmers of the QCed reads in new directory with the name "*final.contigs.fa*".

#### MaSuRCA ####

Documentation tbd - currently is skipped

Supposidly this is a better long read assembler.

#### blast ####

blast the resulting contigs and visualize the results. The user will need the blast database in order for this to be reporducable. The nt database is upwards of 90Gb.

Results may be viewed with the following command

```{bash}
grep -viE "homo|Human|Gorilla|troglodytes|pongo|Macaca|leucogenys|18S" *.blastout
```


