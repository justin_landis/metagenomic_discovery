
#!/bin/bash -l

REDUCE_MEM="T"
MEM=155000

module load bbmap -q

QUEUE_ENV="sbatch"
NUM_PROCS=48
TIME="8:00:00"
BBM_PATH=`which bbmap.sh`
REF_GENOME="all_silva_ribo"
BBM_INDEXES="/home/public/reference_genomes/${REF_GENOME}"
#BBM_INDEXES="/nas/longleaf/home/dnavinci/pine_scratch/allribo"
BBM_ARGS="unpigz=t pigz=t t=${NUM_PROCS}"

# check if BBMap is working
if [ ! -x $BBM_PATH ]; then
echo "BBMap not loaded correctly. Please check that $BBM_PATH exists"
	exit 1
fi

# check if there are even any files given
if [ $# -eq 0 ]; then
echo "Please provide at least one fastq.gz!"
	exit 1
fi

if [[ $REDUCE_MEM == T ]]; then
BBM_ARGS="$BBM_ARGS usemodulo=t Xmx153g"
fi

#perhaps- while [[ $# -gt 1 ]]
for filename in ${@}; do
key="$1"
case $key in
        # parse -n into the number of processors to use
        -f|--force)
		delete_existing=1
	
	;;
	*)  # treat everything else as a filename
        	if [[ ! -f $1 ]];
		then
			echo "ERROR: $1 is not a file"
			exit
		else
			dname=$(dirname ${1})
			# if ends in .gz...
			if [ ${1: -3} == ".gz" ];
			then
				fname=$(basename ${filename} .fastq.gz)
				in="${dname}/${fname}.fastq.gz"
				out="${dname}/${fname}_ribo_depleted.fastq.gz"
			else
				fname=$(basename ${filename} .fastq)
				in="${dname}/${fname}.fastq"
				out="${dname}/${fname}_ribo_depleted.fastq.gz"
			fi
		fi

                # if output file already exists, just exit
                if [[ -f $out ]]; then
			if [[ $delete_existing == 1 ]]; then
				BBM_ARGS="$BBM_ARGS overwirte=t"
			else
        	                echo "ERROR: $out already exists! Use -f to force overwrite."
	                       	exit
			fi
                fi

                #make sure we have real files
                if [[ ! -f ${in} ]]; then
                       	echo "file ${in} does not exist"
                        exit
		else
			# Build up the submission command in the original format:
			# bbmap.sh in=all_together.fastq outu=${out} path=/proj/seq/data/hg38_UCSC/Sequence/BBMapIndex
			submission="${BBM_PATH} ${BBM_ARGS} path=${BBM_INDEXES} in=${in} outu=${out} &>${fname}_ribo_depleted.log"
#                      	ref=${BBM_INDEXES}/${REF_GENOME}.fasta

			echo $submission
		fi
            ;;
esac
shift
done
