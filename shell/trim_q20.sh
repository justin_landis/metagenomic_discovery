#!/bin/bash -l

# EDIT ME! ---- Identify where my adapter file is.
#In Penguin. Used $locate adapters.fa
#the following path was returned
ADAPTER_PATH="~/metagenomic_discovery/Coronavirus_amplicons.fasta"

#ERROR --- "module command not found"

module load --quiet --expert bbmap

BBDUK_PATH=`which bbduk.sh`
BBDUK_ARGS="-Xmx12g qtrim=rl trimq=20 maq=20 minlen=40 tpe tbo" #ktrim defaults to throw it away?
num_procs=12

if [[ -f $ADAPTER_PATH ]]; then
echo "ERROR: $out does not exist"
exit 1
fi

# check if there are even any files given
if [ $# -eq 0 ]; then
       echo "Please provide at least one fastq!"
     fi
     
     for filename in ${@}; do
     key="$1"
     case $key in
     # parse -n into the number of processors to use
     #		-n|--numprocs)
     #			numprocs="$2"
     #			shift
     #		;;
     -f|--force)
delete_existing=1
;;
*)  # treat everything else as a filename
#dname=$(dirname ${filename})  #dname will be the directory path for each "filename"
#fname=$(basename ${filename} .fastq.gz)  #fname will be "filename.fastq.gz"
#make sure we have real files
if [[ -f $1 ]]; then   
    dname=$(dirname ${filename})
    if [ ${1: -3} == ".gz" ]
    then
        fname=$(basename ${filename} .fastq.gz) 
        in=${dname}/${fname}.fastq.gz
        out=${dname}/${fname}_trimQ20.fastq.gz
    else
        fname=$(basename ${filename} .fastq)
        in=${dname}/${fname}.fastq
        out=${dname}/${fname}_trimQ20.fastq
    fi
fi
# if output file already exists, just exit
if [[ -f $out ]]; then
    if [[ $delete_existing == 1 ]]; then
        BBDUK_ARGS="${BBDUK_ARGS} overwrite=t"
    else
        echo "ERROR: $out already exists! Use -f to force overwrite"
        exit 1
    fi
fi
;;
esac
shift  #Indexes over the rest of the arguments

submission="${BBDUK_PATH} ${BBDUK_ARGS} in=${in} out=${out} &> ${fname}_trimQ20.log"
if [[ -f ${in} ]]; then
echo $submission
fi
done
